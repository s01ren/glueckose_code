#!/bin/bash
# Update uberspace > 1.0.0
#   with this script you can update your self-hosted
#   ghost installation on an uberspace server. 
#
# Please change the following parts:
#   line 10 & 32: path to service directory
#   line 14: path to your ghost installation


# stop service
svc -d ~/service/ghost

# change to ghost directory
cd ~/ghost

# backup config
cp core/server/config/env/config.production.json core/server/config/env/config.production.json_bak

# get latest ghost version
curl -L https://ghost.org/zip/ghost-latest.zip -o ghost.zip
unzip -o ghost.zip && rm ghost.zip

# restore configuration
mv -f core/server/config/env/config.production.json_bak core/server/config/env/config.production.json

# update dependencies & database
npm install --python="usr/local/bin/python2.7" --production
NODE_ENV=production knex-migrator migrate

# restart ghost
svc -du ~/service/ghost
